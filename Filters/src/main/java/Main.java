import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import util.CanvasUtils;
import util.ImageUtils;

public class Main extends Component {
  private static final int WIDTH = 1000;
  private static final int HEIGHT = 1000;
  private static final int CANVAS_HEIGHT = 800;
  private static BufferedImage image;

  public static void main(String[] args) {
    JFrame window = new JFrame("Filters");
    window.setLayout(new BorderLayout());

    JPanel menuButton = new JPanel();
    menuButton.setLayout(new GridLayout(4, 5));
    window.add(menuButton, BorderLayout.PAGE_START);

    JButton openFileBtn = new JButton("Open file");
    menuButton.add(openFileBtn);
    JButton clearCanvasBtn = new JButton("Clear");
    menuButton.add(clearCanvasBtn);
    JButton addFilterBtn = new JButton("Add");
    menuButton.add(addFilterBtn);
    JButton subtractFilterBtn = new JButton("Subtract");
    menuButton.add(subtractFilterBtn);
    JButton multiplyFilterBtn = new JButton("Multiply");
    menuButton.add(multiplyFilterBtn);
    JButton divideFilterBtn = new JButton("Divide");
    menuButton.add(divideFilterBtn);
    JButton brightenFilterBtn = new JButton("Brighten");
    menuButton.add(brightenFilterBtn);
    JButton darkenFilterBtn = new JButton("Darken");
    menuButton.add(darkenFilterBtn);
    JButton monochromeFilterBtn = new JButton("Monochrome");
    menuButton.add(monochromeFilterBtn);
    JButton avgFilterBtn = new JButton("Average");
    menuButton.add(avgFilterBtn);
    JButton medianFilterBtn = new JButton("Median");
    menuButton.add(medianFilterBtn);
    JButton sharpenFilterBtn = new JButton("Sharpen");
    menuButton.add(sharpenFilterBtn);
    JButton gaussFilterBtn = new JButton("Gauss");
    menuButton.add(gaussFilterBtn);
    JButton sobelVerticalFilterBtn = new JButton("Sobel vertical");
    menuButton.add(sobelVerticalFilterBtn);
    JButton sobelHorizontalFilterBtn = new JButton("Sobel horizontal");
    menuButton.add(sobelHorizontalFilterBtn);
    JTextField textField = new JTextField(5);
    menuButton.add(textField);

    final JFileChooser fc = new JFileChooser();
    fc.setAcceptAllFileFilterUsed(false);
    FileNameExtensionFilter filter = new FileNameExtensionFilter("PNG files", "png");
    fc.addChoosableFileFilter(filter);
    JPanel canvas = new JPanel();
    canvas.setPreferredSize(new Dimension(WIDTH, CANVAS_HEIGHT));
    canvas.setSize(new Dimension(WIDTH, 600));
    window.add(canvas);
    window.setSize(new Dimension(WIDTH, HEIGHT));
    window.setVisible(true);
    window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    sobelHorizontalFilterBtn.addActionListener(
        e -> {
          Filters.sobelHorizontal(image, 3, 3);
          updateImage(canvas);
        });

    sobelVerticalFilterBtn.addActionListener(
        e -> {
          Filters.sobelVertical(image, 3, 3);
          updateImage(canvas);
        });

    sharpenFilterBtn.addActionListener(
        e -> {
          Filters.sharpen(image, 3, 3);
          updateImage(canvas);
        });

    gaussFilterBtn.addActionListener(
        e -> {
          Filters.gauss(image, 3, 3);
          updateImage(canvas);
        });

    medianFilterBtn.addActionListener(
        e -> {
          int w = Integer.parseInt(textField.getText());
          Filters.median(image, w, w);
          updateImage(canvas);
        });

    avgFilterBtn.addActionListener(
        e -> {
          int w = Integer.parseInt(textField.getText());
          Filters.average(image, w, w);
          updateImage(canvas);
        });

    addFilterBtn.addActionListener(
        e -> {
          int n = Integer.parseInt(textField.getText());
          PointTransformationsFactory.add(image, n);
          updateImage(canvas);
        });

    subtractFilterBtn.addActionListener(
        e -> {
          int n = Integer.parseInt(textField.getText());
          PointTransformationsFactory.subtract(image, n);
          updateImage(canvas);
        });

    multiplyFilterBtn.addActionListener(
        e -> {
          int n = Integer.parseInt(textField.getText());
          PointTransformationsFactory.multiply(image, n);
          updateImage(canvas);
        });

    divideFilterBtn.addActionListener(
        e -> {
          int n = Integer.parseInt(textField.getText());
          PointTransformationsFactory.divide(image, n);
          updateImage(canvas);
        });

    brightenFilterBtn.addActionListener(
        e -> {
          int n = Integer.parseInt(textField.getText());
          PointTransformationsFactory.brighten(image, n);
          updateImage(canvas);
        });

    darkenFilterBtn.addActionListener(
        e -> {
          int n = Integer.parseInt(textField.getText());

          PointTransformationsFactory.darken(image, n);
          updateImage(canvas);
        });

    monochromeFilterBtn.addActionListener(
        e -> {
          PointTransformationsFactory.monochrome(image);
          updateImage(canvas);
        });

    clearCanvasBtn.addActionListener(
        e -> CanvasUtils.clearCanvas(canvas.getGraphics(), WIDTH, CANVAS_HEIGHT));
    openFileBtn.addActionListener(e -> readFile(window, fc, canvas));
  }

  private static void updateImage(JPanel canvas) {
    final Graphics2D graphics2D = (Graphics2D) canvas.getGraphics();
    CanvasUtils.drawImage(graphics2D, image);
  }

  private static void readFile(JFrame window, JFileChooser fc, JPanel canvas) {
    int returnVal = fc.showOpenDialog(window);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
      File file = fc.getSelectedFile();
      image = ImageUtils.readFromFile(file);
      updateImage(canvas);
    }
  }
}
