import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Filters {

  public static void average(BufferedImage image, int maskW, int maskH) {
    int[][] values = new int[image.getWidth()][image.getHeight()];
    for (int i = 0; i < image.getWidth(); i++) {
      for (int j = 0; j < image.getHeight(); j++) {
        if (i < maskW / 2
            || j < maskH / 2
            || i >= image.getWidth() - maskW / 2
            || j >= image.getHeight() - maskH / 2) {
          values[i][j] = new Color(0, 0, 0).getRGB();
          continue;
        }
        values[i][j] = calculateAverage(image, i, j, maskW, maskH);
      }
    }
    for (int i = 0; i < image.getWidth(); i++) {
      for (int j = 0; j < image.getHeight(); j++) {
        image.setRGB(i, j, values[i][j]);
      }
    }
  }

  private static int calculateAverage(BufferedImage image, int x, int y, int maskW, int maskH) {
    int sumRed = 0;
    int sumGreen = 0;
    int sumBlue = 0;
    for (int i = x - maskW / 2; i < x + maskW / 2 + 1; i++) {
      for (int j = y - maskH / 2; j < y + maskH / 2 + 1; j++) {
        final Color color = new Color(image.getRGB(i, j));
        sumRed += color.getRed();
        sumGreen += color.getGreen();
        sumBlue += color.getBlue();
      }
    }
    int maskSum = maskW * maskH;
    int red = Math.min(sumRed / maskSum, 255);
    int green = Math.min(sumGreen / maskSum, 255);
    int blue = Math.min(sumBlue / maskSum, 255);
    return new Color(red, green, blue).getRGB();
  }

  public static void median(BufferedImage image, int maskW, int maskH) {
    int[][] values = new int[image.getWidth()][image.getHeight()];

    for (int i = 0; i < image.getWidth(); i++) {
      for (int j = 0; j < image.getHeight(); j++) {
        if (i < maskW / 2
            || j < maskH / 2
            || i >= image.getWidth() - maskW / 2
            || j >= image.getHeight() - maskH / 2) {
          values[i][j] = new Color(0, 0, 0).getRGB();
          continue;
        }
        values[i][j] = calculateMedian(image, i, j, maskW, maskH);
      }
    }
    for (int i = 0; i < image.getWidth(); i++) {
      for (int j = 0; j < image.getHeight(); j++) {
        image.setRGB(i, j, values[i][j]);
      }
    }
  }

  private static int calculateMedian(BufferedImage image, int x, int y, int maskW, int maskH) {
    List<Integer> colors = new ArrayList<>();
    for (int i = x - maskW / 2; i < x + maskW / 2 + 1; i++) {
      for (int j = y - maskH / 2; j < y + maskH / 2 + 1; j++) {
        colors.add(image.getRGB(i, j));
      }
    }
    colors.sort(Comparator.comparingInt(a -> a));

    return colors.get(colors.size() / 2);
  }

  public static void sharpen(BufferedImage image, int maskW, int maskH) {
    int[][] values = new int[image.getWidth()][image.getHeight()];

    for (int i = 0; i < image.getWidth(); i++) {
      for (int j = 0; j < image.getHeight(); j++) {
        if (i < maskW / 2
            || j < maskH / 2
            || i >= image.getWidth() - maskW / 2
            || j >= image.getHeight() - maskH / 2) {
          values[i][j] = new Color(0, 0, 0).getRGB();
          continue;
        }
        values[i][j] = calculateSharpening(image, i, j, maskW, maskH);
      }
    }
    for (int i = 0; i < image.getWidth(); i++) {
      for (int j = 0; j < image.getHeight(); j++) {
        image.setRGB(i, j, values[i][j]);
      }
    }
  }

  private static int calculateSharpening(BufferedImage image, int x, int y, int maskW, int maskH) {
    int[][] mask = {{0, -1, 0}, {-1, 5, -1}, {0, -1, 0}};
    int sumRed = 0;
    int sumGreen = 0;
    int sumBlue = 0;

    int maskI = 0;
    int maskJ = 0;

    for (int i = x - maskW / 2; i < x + maskW / 2 + 1; i++) {
      for (int j = y - maskH / 2; j < y + maskH / 2 + 1; j++) {
        final Color color = new Color(image.getRGB(i, j));
        sumRed += color.getRed() * mask[maskI][maskJ];
        sumGreen += color.getGreen() * mask[maskI][maskJ];
        sumBlue += color.getBlue() * mask[maskI][maskJ];
        maskI++;
      }
      maskI = 0;
      maskJ++;
    }

    return new Color(
            sumRed > 0 ? Math.min(sumRed, 255) : 0,
            sumGreen > 0 ? Math.min(sumGreen, 255) : 0,
            sumBlue > 0 ? Math.min(sumBlue, 255) : 0)
        .getRGB();
  }

  public static void gauss(BufferedImage image, int maskW, int maskH) {
    int[][] values = new int[image.getWidth()][image.getHeight()];

    for (int i = 0; i < image.getWidth(); i++) {
      for (int j = 0; j < image.getHeight(); j++) {
        if (i < maskW / 2
            || j < maskH / 2
            || i >= image.getWidth() - maskW / 2
            || j >= image.getHeight() - maskH / 2) {
          values[i][j] = new Color(0, 0, 0).getRGB();
          continue;
        }
        values[i][j] = calculateGauss(image, i, j, maskW, maskH);
      }
    }
    for (int i = 0; i < image.getWidth(); i++) {
      for (int j = 0; j < image.getHeight(); j++) {
        image.setRGB(i, j, values[i][j]);
      }
    }
  }

  private static int calculateGauss(BufferedImage image, int x, int y, int maskW, int maskH) {
    int[][] mask = {{1, 2, 1}, {2, 4, 2}, {1, 2, 1}};
    int sumRed = 0;
    int sumGreen = 0;
    int sumBlue = 0;

    int maskI = 0;
    int maskJ = 0;

    for (int i = x - maskW / 2; i < x + maskW / 2 + 1; i++) {
      for (int j = y - maskH / 2; j < y + maskH / 2 + 1; j++) {
        final Color color = new Color(image.getRGB(i, j));
        sumRed += color.getRed() * mask[maskI][maskJ];
        sumGreen += color.getGreen() * mask[maskI][maskJ];
        sumBlue += color.getBlue() * mask[maskI][maskJ];
        maskI++;
      }
      maskI = 0;
      maskJ++;
    }

    return new Color(sumRed / 16, sumGreen / 16, sumBlue / 16).getRGB();
  }

  public static void sobelVertical(BufferedImage image, int maskW, int maskH) {
    int[][] values = new int[image.getWidth()][image.getHeight()];

    for (int i = 0; i < image.getWidth(); i++) {
      for (int j = 0; j < image.getHeight(); j++) {
        if (i < maskW / 2
            || j < maskH / 2
            || i >= image.getWidth() - maskW / 2
            || j >= image.getHeight() - maskH / 2) {
          values[i][j] = new Color(0, 0, 0).getRGB();
          continue;
        }
        values[i][j] = calculateSobelVertical(image, i, j, maskW, maskH);
      }
    }
    for (int i = 0; i < image.getWidth(); i++) {
      for (int j = 0; j < image.getHeight(); j++) {
        image.setRGB(i, j, values[i][j]);
      }
    }
  }

  private static int calculateSobelVertical(
      BufferedImage image, int x, int y, int maskW, int maskH) {
    int[][] mask = {{1, 0, -1}, {2, 0, -2}, {1, 0, -1}};
    int sumRed = 0;
    int sumGreen = 0;
    int sumBlue = 0;

    int maskI = 0;
    int maskJ = 0;

    for (int i = x - maskW / 2; i < x + maskW / 2 + 1; i++) {
      for (int j = y - maskH / 2; j < y + maskH / 2 + 1; j++) {
        final Color color = new Color(image.getRGB(i, j));
        sumRed += color.getRed() * mask[maskI][maskJ];
        sumGreen += color.getGreen() * mask[maskI][maskJ];
        sumBlue += color.getBlue() * mask[maskI][maskJ];
        maskI++;
      }
      maskI = 0;
      maskJ++;
    }

    return new Color(
            sumRed > 0 ? Math.min(sumRed, 255) : 0,
            sumGreen > 0 ? Math.min(sumGreen, 255) : 0,
            sumBlue > 0 ? Math.min(sumBlue, 255) : 0)
        .getRGB();
  }

  public static void sobelHorizontal(BufferedImage image, int maskW, int maskH) {
    int[][] values = new int[image.getWidth()][image.getHeight()];

    for (int i = 0; i < image.getWidth(); i++) {
      for (int j = 0; j < image.getHeight(); j++) {
        if (i < maskW / 2
            || j < maskH / 2
            || i >= image.getWidth() - maskW / 2
            || j >= image.getHeight() - maskH / 2) {
          values[i][j] = new Color(0, 0, 0).getRGB();
          continue;
        }
        values[i][j] = calculateSobelHorizontal(image, i, j, maskW, maskH);
      }
    }
    for (int i = 0; i < image.getWidth(); i++) {
      for (int j = 0; j < image.getHeight(); j++) {
        image.setRGB(i, j, values[i][j]);
      }
    }
  }

  private static int calculateSobelHorizontal(
      BufferedImage image, int x, int y, int maskW, int maskH) {
    int[][] mask = {{1, 2, 1}, {0, 0, 0}, {-1, -2, -1}};
    int sumRed = 0;
    int sumGreen = 0;
    int sumBlue = 0;

    int maskI = 0;
    int maskJ = 0;

    for (int i = x - maskW / 2; i < x + maskW / 2 + 1; i++) {
      for (int j = y - maskH / 2; j < y + maskH / 2 + 1; j++) {
        final Color color = new Color(image.getRGB(i, j));
        sumRed += color.getRed() * mask[maskI][maskJ];
        sumGreen += color.getGreen() * mask[maskI][maskJ];
        sumBlue += color.getBlue() * mask[maskI][maskJ];
        maskI++;
      }
      maskI = 0;
      maskJ++;
    }

    return new Color(
            sumRed > 0 ? Math.min(sumRed, 255) : 0,
            sumGreen > 0 ? Math.min(sumGreen, 255) : 0,
            sumBlue > 0 ? Math.min(sumBlue, 255) : 0)
        .getRGB();
  }
}
