import java.awt.*;
import java.awt.image.BufferedImage;

public class PointTransformationsFactory {

  public static void add(BufferedImage image, int n) {
    for (int i = 0; i < image.getWidth(); i++) {
      for (int j = 0; j < image.getHeight(); j++) {
        final Color rgb = new Color(image.getRGB(i, j));
        int red = performAddition(n, rgb.getRed());
        int green = performAddition(n, rgb.getGreen());
        int blue = performAddition(n, rgb.getBlue());
        image.setRGB(i, j, new Color(red, green, blue).getRGB());
      }
    }
  }

  private static int performAddition(int n, int color) {
    return (color + n) % 256;
  }

  public static void subtract(BufferedImage image, int n) {
    for (int i = 0; i < image.getWidth(); i++) {
      for (int j = 0; j < image.getHeight(); j++) {
        final Color rgb = new Color(image.getRGB(i, j));
        int red = performSubtraction(n, rgb.getRed());
        int green = performSubtraction(n, rgb.getGreen());
        int blue = performSubtraction(n, rgb.getBlue());
        image.setRGB(i, j, new Color(red, green, blue).getRGB());
      }
    }
  }

  private static int performSubtraction(int n, int color) {
    if (n <= color) {
      return color - n;
    }
    return color;
  }

  public static void multiply(BufferedImage image, int n) {
    for (int i = 0; i < image.getWidth(); i++) {
      for (int j = 0; j < image.getHeight(); j++) {
        final Color rgb = new Color(image.getRGB(i, j));
        int red = performMultiplication(n, rgb.getRed());
        int green = performMultiplication(n, rgb.getGreen());
        int blue = performMultiplication(n, rgb.getBlue());
        image.setRGB(i, j, new Color(red, green, blue).getRGB());
      }
    }
  }

  private static int performMultiplication(int n, int color) {
    return (color * n) % 256;
  }

  public static void divide(BufferedImage image, int n) {
    for (int i = 0; i < image.getWidth(); i++) {
      for (int j = 0; j < image.getHeight(); j++) {
        final Color rgb = new Color(image.getRGB(i, j));
        int red = performDivision(n, rgb.getRed());
        int green = performDivision(n, rgb.getGreen());
        int blue = performDivision(n, rgb.getBlue());
        image.setRGB(i, j, new Color(red, green, blue).getRGB());
      }
    }
  }

  private static int performDivision(int n, int color) {
    if (n <= 0) {
      return color;
    }
    return (int) (Math.floor(color / (n * 1.0)) % 256);
  }

  public static void darken(BufferedImage image, double n) {
    for (int i = 0; i < image.getWidth(); i++) {
      for (int j = 0; j < image.getHeight(); j++) {
        final Color rgb = new Color(image.getRGB(i, j));
        int red = mapToColorValue(performDarkening(n, rgb.getRed()), Math.pow(255, 2));
        int green = mapToColorValue(performDarkening(n, rgb.getGreen()), Math.pow(255, 2));
        int blue = mapToColorValue(performDarkening(n, rgb.getBlue()), Math.pow(255, 2));
        if(red > 255) red = 255;
        if(green > 255) green = 255;
        if(blue > 255) blue = 255;
        image.setRGB(i, j, new Color(red, green, blue).getRGB());
      }
    }
  }

  private static int performDarkening(double c, int color) {
    return (int) (c * Math.pow(color, 2));
  }

  public static void brighten(BufferedImage image, double n) {
    for (int i = 0; i < image.getWidth(); i++) {
      for (int j = 0; j < image.getHeight(); j++) {
        final Color rgb = new Color(image.getRGB(i, j));
        int red = mapToColorValue(performBrightening(n, rgb.getRed()), Math.log(256) * n);
        int green = mapToColorValue(performBrightening(n, rgb.getGreen()), Math.log(256) * n);
        int blue = mapToColorValue(performBrightening(n, rgb.getBlue()), Math.log(256) * n);
        image.setRGB(i, j, new Color(red, green, blue).getRGB());
      }
    }
  }

  private static double performBrightening(double c, int color) {
    return c * Math.log(color + 1);
  }

  private static int mapToColorValue(double input, double input_end) {
    return (int) ((255 / input_end) * input);
  }

  public static void monochrome(BufferedImage image) {
    for (int i = 0; i < image.getWidth(); i++) {
      for (int j = 0; j < image.getHeight(); j++) {
        final Color rgb = new Color(image.getRGB(i, j));
        int red = rgb.getRed();
        image.setRGB(i, j, new Color(red, red, red).getRGB());
      }
    }
  }
}
